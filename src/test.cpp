#include <iostream>
#include <thread>
#include <chrono>
#include "tcp_server.h"
#include "io_buffer.h"

#ifdef __linux__
#include <signal.h>
#endif

#ifdef _WIN32
#pragma comment(lib, "ws2_32.lib")
#endif


static bool g_exit = false;

class TestTcpClient : public TcpServerClient
{
public:
	TestTcpClient(SocketHandle server_fd, SocketHandle client_fd) : TcpServerClient(server_fd, client_fd)
	{}

	virtual void on_connect(const std::string& host, uint16_t port)
	{
		std::cout << "client connect, handle: " << client_socket() << ", host: " << host << ", port: " << port << std::endl;
	}

	virtual void on_disconnect()
	{
		std::cout << "client disconnect, handle: " << client_socket() << std::endl;
	}

	virtual void on_data(IOBuffer* io_buffer)
	{
		// called via multiple threadpool threads, protect SPSC IO Buffer read
		std::unique_lock<std::mutex> lock(m_mutex);
		uint32_t size;
		auto p = try_read_io_buffer(io_buffer, &size);
		if(p)
		{
			std::cout << (const char*)p;
			read_io_buffer(io_buffer);
			Send(p, size);
		}
	}
};


TcpServer 	g_tcp_server;

#ifdef __linux__

void int_handler(int) {
    g_exit = true;
}

#endif

#ifdef _WIN32

BOOL WINAPI ConsoleHandler(DWORD event)
{
	if(event == CTRL_C_EVENT) {
		g_exit = true;
	}
	return TRUE;
}

#endif

int main()
{
#ifdef __linux__
	signal(SIGINT, int_handler);
#endif

#ifdef _WIN32
	SetConsoleCtrlHandler((PHANDLER_ROUTINE)ConsoleHandler, TRUE);

	WSADATA wsa_data;
	WSAStartup(0x0202, &wsa_data);
#endif
	g_tcp_server.set_client_creator<TestTcpClient>();

	const uint16_t port = 6666;
	g_tcp_server.start(2, port);
	std::cout << "Running at port " << port << ", Ctrl+C to break" << std::endl;
	while(!g_exit) {
		std::this_thread::sleep_for(std::chrono::milliseconds(50));
	}
	g_tcp_server.stop();

#ifdef _WIN32
	//WSACleanup();
#endif
	return 0;
}
