#ifdef __linux__
#include <fcntl.h>
#include <errno.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/epoll.h>
#include <unistd.h>
#endif
#ifdef _WIN32
#include <WS2tcpip.h>
#endif
#include <string.h>
#include <chrono>
#include <iostream>
#include "tcp_server.h"
#include "io_buffer.h"
#include "threadpool.h"

#define MAXEVENTS		64
#define IO_BUFFER_SIZE	1024*16

#ifdef _WIN32
#pragma warning( disable : 4316 )
#pragma warning( disable : 4996 )

struct IOCPContext
{
	OVERLAPPED      overlapped;
	SOCKET          client_fd;
	DWORD			accept_bytes;
	char			buffer[1024];
	int             action;

	void reset()
	{
		memset(&overlapped, 0, sizeof(overlapped));
		memset(buffer, 0, sizeof(buffer));
		accept_bytes = 0;
	}
};

#define IOCP_ACTION_ACCEPT		1
#define IOCP_ACTION_READ		2
#endif

TcpServerClient::TcpServerClient(SocketHandle server_fd, SocketHandle client_fd)
{
	m_server_fd = server_fd;
	m_client_fd = client_fd;
}

TcpServerClient::~TcpServerClient()
{
	CloseClientSocket();
}

void TcpServerClient::CloseClientSocket()
{
	if(m_client_fd < 0) {
		return;
	}

#ifdef __linux__
	close(m_client_fd);
#endif

#ifdef _WIN32
	closesocket(m_client_fd);
#endif

	m_client_fd = -1;
}

void TcpServerClient::on_connect(const std::string& host, uint16_t port)
{
	(void)host;
	(void)port;
}

void TcpServerClient::on_disconnect()
{
}

void TcpServerClient::on_data(IOBuffer* io_buffer)
{
	// called via multiple threadpool threads, protect SPSC IO Buffer read
	std::unique_lock<std::mutex> lock(m_mutex);
	uint32_t size = 0;
	auto p = try_read_io_buffer(io_buffer, &size);
	if(p) {
		read_io_buffer(io_buffer);
	}
	std::cout << "connection handle: " << m_client_fd << ", data size: " << size << std::endl;
}

int TcpServerClient::Send(const uint8_t* data, uint32_t data_size)
{
#ifdef __linux__
	return write(client_socket(), data, data_size);
#endif

#ifdef _WIN32
	WSABUF wsa_buf;
	wsa_buf.buf = (CHAR*)data;
	wsa_buf.len = data_size;
	return WSASend(client_socket(), &wsa_buf, 1, NULL, 0, NULL, NULL);
#endif
}


TcpServer::TcpServer()
{
	m_thread_pool = nullptr;

	m_running = false;
	m_exit = false;

#ifdef __linux__
	m_socket_fd = -1;
	m_epoll_fd = -1;
#endif

#ifdef _WIN32
	m_socket_fd = INVALID_SOCKET;
	m_iocp = INVALID_HANDLE_VALUE;
	m_fn_AcceptEx = nullptr;
	m_fn_GetAcceptExSockAddrs = nullptr;
#endif
}

TcpServer::~TcpServer()
{
	stop();
}

void TcpServer::set_stop()
{
	m_exit = true;
#ifdef _WIN32
	if(m_iocp != INVALID_HANDLE_VALUE) {
		PostQueuedCompletionStatus(m_iocp, 0, 0, NULL);
	}
#endif
}

void TcpServer::stop()
{
	m_exit = true;
#ifdef _WIN32
	if(m_iocp != INVALID_HANDLE_VALUE) {
		PostQueuedCompletionStatus(m_iocp, 0, 0, NULL);
	}
#endif

	while(m_running)
	{
#if !defined(__GNUC__) || __GNUC__ >= 5
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
#else
		usleep(10000);
#endif
	}
}

void TcpServer::set_client_creator(std::function<TcpServerClient* (SocketHandle, SocketHandle)> creator)
{
	m_client_creator = creator;
}

TcpServerClient* TcpServer::new_client(SocketHandle client_fd) {
	if(m_client_creator) {
		return m_client_creator(m_socket_fd, client_fd);
	}
	return new TcpServerClient(m_socket_fd, client_fd);
}

bool TcpServer::init(const std::string& host, uint16_t port, std::string& error_function)
{
	error_function.clear();

#ifdef __linux__
	int arg = 1;
	m_epoll_fd = -1;

	// Create the listening socket
	m_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
	if(m_socket_fd == -1)
	{
		error_function = "socket";
		goto err;
	}

	// set SO_REUSEADDR so port can be resused imemediately after exit, i.e., after CTRL-c
	if(setsockopt(m_socket_fd, SOL_SOCKET, SO_REUSEADDR, &arg, sizeof(arg)) == -1)
	{
		error_function = "setsockopt";
		goto err;
	}

	// Make the server listening socket non-blocking
	if(fcntl(m_socket_fd, F_SETFL, O_NONBLOCK | fcntl(m_socket_fd, F_GETFL, 0)) == -1)
	{
		error_function = "fcntl";
		goto err;
	}

	struct hostent* he;

	if((he = gethostbyname(host.c_str())) == NULL)
	{
		error_function = "gethostbyname";
		goto err;
	}

	struct sockaddr_in addr;
	memset(&addr, 0, sizeof(struct sockaddr_in));
	addr.sin_family = AF_INET;
	memcpy(&addr.sin_addr, he->h_addr_list[0], he->h_length);
	addr.sin_port = htons(port);
	if(bind(m_socket_fd, (struct sockaddr*)&addr, sizeof(addr)) == -1)
	{
		error_function = "bind";
		goto err;
	}

	// Create the epoll file descriptor
	m_epoll_fd = epoll_create(MAXEVENTS);
	if(m_epoll_fd == -1)
	{
		error_function = "epoll_create";
		goto err;
	}

	// Listen for fd_news; SOMAXCONN is 128 by default
	if(listen(m_socket_fd, SOMAXCONN) == -1)
	{
		error_function = "listen";
		goto err;
	}

	// Add the server socket to the epoll event loop
	struct epoll_event event;
	event.data.fd = m_socket_fd;
	event.events = EPOLLIN | EPOLLET;
	if(epoll_ctl(m_epoll_fd, EPOLL_CTL_ADD, m_socket_fd, &event) == -1)
	{
		error_function = "epoll_ctl";
		goto err;
	}

	return true;

err:
	if(m_socket_fd != -1)
	{
		close(m_socket_fd);
		m_socket_fd = -1;
	}

	if(m_epoll_fd != -1)
	{
		close(m_epoll_fd);
		m_epoll_fd = -1;
	}
	return false;
#endif

#ifdef _WIN32
	static GUID ax_guid = WSAID_ACCEPTEX;
	static GUID as_guid = WSAID_GETACCEPTEXSOCKADDRS;

	DWORD ret;

	auto sock = socket(AF_INET, SOCK_STREAM, IPPROTO_IP);
	if(sock == -1)
	{
		error_function = "socket";
		goto err;
	}

	if(m_fn_AcceptEx == nullptr && -1 == WSAIoctl(sock, SIO_GET_EXTENSION_FUNCTION_POINTER, &ax_guid, sizeof(GUID),
		&m_fn_AcceptEx, sizeof(LPFN_ACCEPTEX), &ret, NULL, NULL))
	{
		error_function = "WSAIoctl(SIO_GET_EXTENSION_FUNCTION_POINTER)";
		goto err;
	}

	if(m_fn_GetAcceptExSockAddrs == nullptr && -1 == WSAIoctl(sock, SIO_GET_EXTENSION_FUNCTION_POINTER, &as_guid, sizeof(GUID),
		&m_fn_GetAcceptExSockAddrs, sizeof(LPFN_GETACCEPTEXSOCKADDRS),
		&ret, NULL, NULL))
	{
		error_function = "WSAIoctl(SIO_GET_EXTENSION_FUNCTION_POINTER)";
		goto err;
	}

	closesocket(sock);

	if((m_iocp = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, 0, 0)) == NULL)
	{
		error_function = "CreateIoCompletionPort";
		goto err;
	}

	m_socket_fd = WSASocketW(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
	if(m_socket_fd == INVALID_SOCKET)
	{
		error_function = "WSASocketW";
		goto err;
	}

	int opt_val = 1;
	int opt_len = sizeof(int);
	if(-1 == setsockopt(m_socket_fd, SOL_SOCKET, SO_KEEPALIVE, (const char*)&opt_val, opt_len))
	{
		error_function = "setsockopt(SO_KEEPALIVE)";
		goto err;
	}

	/*if(-1 == setsockopt(m_socket_fd, SOL_SOCKET, SO_REUSEADDR, (const char*)&opt_val, opt_len))
	{
		error_function = "setsockopt(SO_REUSEADDR)";
		goto err;
	}*/

	if(INVALID_HANDLE_VALUE == CreateIoCompletionPort((HANDLE)m_socket_fd, m_iocp, 0, 0))
	{
		error_function = "CreateIoCompletionPort";
		goto err;
	}

	struct hostent* he;
	if((he = gethostbyname(host.c_str())) == NULL)
	{
		error_function = "gethostbyname";
		goto err;
	}

	struct sockaddr_in addr;
	memset(&addr, 0, sizeof(struct sockaddr_in));
	addr.sin_family = AF_INET;
	memcpy(&addr.sin_addr, he->h_addr_list[0], he->h_length);
	addr.sin_port = htons(port);

	if(bind(m_socket_fd, (struct sockaddr*)&addr, sizeof(addr)) == -1)
	{
		error_function = "bind";
		goto err;
	}

	// Listen for fd_news; SOMAXCONN is 128 by default
	if(listen(m_socket_fd, SOMAXCONN) == -1)
	{
		error_function = "listen";
		goto err;
	}

	return true;

err:
	m_fn_AcceptEx = nullptr;
	m_fn_GetAcceptExSockAddrs = nullptr;

	if(m_socket_fd != INVALID_SOCKET)
	{
		closesocket(m_socket_fd);
		m_socket_fd = INVALID_SOCKET;
	}

	if(sock != INVALID_SOCKET) {
		closesocket(sock);
	}

	return false;
#endif
}

#ifdef _WIN32
bool TcpServer::iocp_post_accept(IOCPContext* iocp_ctx)
{
	iocp_ctx->reset();
	iocp_ctx->action = IOCP_ACTION_ACCEPT;

	iocp_ctx->client_fd = WSASocketW(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
	if(INVALID_SOCKET == iocp_ctx->client_fd)
	{
		on_run_error("WSASocketW(WSA_FLAG_OVERLAPPED)");
		return false;
	}

	BOOL result = m_fn_AcceptEx(
		m_socket_fd,
		iocp_ctx->client_fd,
		iocp_ctx->buffer,
		0,
		sizeof(SOCKADDR_IN) + 16,
		sizeof(SOCKADDR_IN) + 16,
		&iocp_ctx->accept_bytes,
		&(iocp_ctx->overlapped)
	);
	if(0 == result)
	{
		if(WSA_IO_PENDING != WSAGetLastError()) {
			return false;
		}
	}

	return true;
}

bool TcpServer::iocp_post_recv(IOCPContext* iocp_ctx)
{
	DWORD flags = 0;
	WSABUF wsa_buf;
	wsa_buf.buf = iocp_ctx->buffer;
	wsa_buf.len = sizeof(iocp_ctx->buffer);
	iocp_ctx->action = IOCP_ACTION_READ;
	WSARecv(iocp_ctx->client_fd, &wsa_buf, 1, NULL, &flags, &iocp_ctx->overlapped, NULL);
	return true;
}

void TcpServer::accept_connection(IOCPContext* iocp_ctx)
{
	SOCKADDR_IN* local_sock_addr = NULL;
	SOCKADDR_IN* remote_sock_addr = NULL;
	int addr_len = sizeof(SOCKADDR_IN);
	//char host[NI_MAXHOST] = { 0 };
	//char port[NI_MAXSERV] = { 0 };
	m_fn_GetAcceptExSockAddrs(
		iocp_ctx->buffer,
		iocp_ctx->accept_bytes,
		addr_len + 16,
		addr_len + 16,
		(SOCKADDR**)&local_sock_addr, &addr_len,
		(SOCKADDR**)&remote_sock_addr, &addr_len
	);

	if(remote_sock_addr)
	{
		char buffer[128] = { 0 };
		inet_ntop(remote_sock_addr->sin_family, &remote_sock_addr->sin_addr, buffer, sizeof(buffer));
		on_connect(buffer, ntohs(remote_sock_addr->sin_port), iocp_ctx->client_fd);
	}
	else {
		on_connect(std::string(), 0, iocp_ctx->client_fd);
	}
}

void TcpServer::close_connection(IOCPContext* iocp_ctx)
{
	if(iocp_ctx->action != IOCP_ACTION_ACCEPT && iocp_ctx->client_fd != INVALID_SOCKET)
	{
		on_disconnect(iocp_ctx->client_fd);
		closesocket(iocp_ctx->client_fd);
	}

	delete iocp_ctx;
}

IOCPContext* TcpServer::iocp_bind_incoming(IOCPContext* iocp_ctx)
{
	IOCPContext* iocp_rw_ctx = new IOCPContext();
	memset(iocp_rw_ctx, 0, sizeof(IOCPContext));
	iocp_rw_ctx->client_fd = iocp_ctx->client_fd;
	iocp_rw_ctx->action = IOCP_ACTION_READ;
	CreateIoCompletionPort((HANDLE)iocp_rw_ctx->client_fd, m_iocp, (ULONG_PTR)iocp_rw_ctx, 0);
	return iocp_rw_ctx;
}

#endif

void TcpServer::on_run_error(const char* error_function)
{
	std::cerr << error_function << " failed" << std::endl;
}

void TcpServer::on_connect(const std::string& host, uint16_t port, SocketHandle client_fd)
{
	auto it = m_clients.find(client_fd);
	if(it == m_clients.end())
	{
		std::shared_ptr<TcpServerClient> client(new_client(client_fd));
		auto result = m_clients.insert(std::make_pair(client_fd, client));
		it = result.first;
	}

	m_thread_pool->enqueue(std::bind(&TcpServerClient::on_connect, it->second, host, port));
}

void TcpServer::on_disconnect(SocketHandle client_fd)
{
	auto it = m_clients.find(client_fd);
	if(it != m_clients.end())
	{
		m_thread_pool->enqueue(std::bind(&TcpServerClient::on_disconnect, it->second));
		m_clients.erase(it);
	}
}

void TcpServer::on_data(SocketHandle client_fd, IOBuffer* io_buffer)
{
	auto it = m_clients.find(client_fd);
	if(it != m_clients.end()) {
		m_thread_pool->enqueue(std::bind(&TcpServerClient::on_data, it->second, io_buffer));
	}
}

bool TcpServer::start(int thread_count, uint16_t port, const std::string& host)
{
	if(is_running()) {
		return true;
	}

	std::string error_function;
	if(!init(host, port, error_function)) {
		return false;
	}

	std::thread t(std::bind(&TcpServer::run, this, thread_count));
	t.detach();
	return true;
}

void TcpServer::run(int thread_count)
{
	m_running = true;

	m_thread_pool = new ThreadPool(thread_count);
	IOBuffer** io_buffers = new IOBuffer*[thread_count];

	for(int i = 0; i < thread_count; i++)
	{
		io_buffers[i] = (IOBuffer*)malloc(sizeof(IOBuffer));
		create_io_buffer(io_buffers[i], IO_BUFFER_SIZE, nullptr);
	}

#ifdef __linux__
	struct epoll_event ctl_event;
	struct epoll_event* events = static_cast<epoll_event*>(calloc(MAXEVENTS, sizeof(epoll_event)));
	struct epoll_event* event;

	uint8_t data[1024];

	int event_count;

	m_exit = false;

	while(!m_exit)
	{
		event_count = epoll_wait(m_epoll_fd, events, MAXEVENTS, 3000);
		if(event_count == 0)
		{
			//timeout
			continue;
		}

		for(int i = 0; i < event_count; i++)
		{
			event = &events[i];

			if((event->events & EPOLLERR) ||
				(event->events & EPOLLHUP) ||
				(!(event->events & EPOLLIN)))
			{
				// An error has occured on this fd, or the socket is not ready for reading (why were we notified then?).
				//fprintf(stderr, "epoll error\n");
				close(event->data.fd);
				on_disconnect(event->data.fd);
			}
			else if(event->events & EPOLLRDHUP)
			{
				// Stream socket peer closed connection, or shut down writing half of connection.
				// We still to handle disconnection when read()/recv() return 0 or -1 just to be sure.
				//printf("Closed connection on descriptor vis EPOLLRDHUP %d\n", event->data.fd);
				// Closing the descriptor will make epoll remove it from the set of descriptors which are monitored.
				close(event->data.fd);
				on_disconnect(event->data.fd);
			}
			else if(m_socket_fd == event->data.fd)
			{
				// We have a notification on the listening socket, which means one or more incoming connections.
				while(true)
				{
					struct sockaddr in_addr;
					socklen_t in_len;
					int infd;

					in_len = sizeof in_addr;
					// No need to make these sockets non blocking since accept4() takes care of it.
#ifdef _WIN32 //!defined(__GNUC__) || __GNUC__ >= 5
					infd = accept4(m_socket_fd, &in_addr, &in_len, SOCK_NONBLOCK | SOCK_CLOEXEC);
#else
					infd = accept(m_socket_fd, &in_addr, &in_len);
#endif
					if(infd == -1)
					{
						if((errno == EAGAIN) || (errno == EWOULDBLOCK)) {
							break;  // We have processed all incoming connections.
						}
						else
						{
							on_run_error("accept");
							break;
						}
					}

					// Make the server listening socket non-blocking
					if(fcntl(infd, F_SETFL, O_NONBLOCK | fcntl(infd, F_GETFL, 0)) == -1)
					{
						on_run_error("fcntl");
						break;
					}

					char host[NI_MAXHOST] = { 0 };
					char port[NI_MAXSERV] = { 0 };
					int retval = getnameinfo(&in_addr, in_len,
						host, sizeof host,
						port, sizeof port,
						NI_NUMERICHOST | NI_NUMERICSERV);
					if(retval == 0) {
						on_connect(host, (uint16_t)atoi(port), infd);
					}
					else {
						on_connect(std::string(), 0, infd);
					}

					// Register the new FD to be monitored by epoll.
					ctl_event.data.fd = infd;
					// Register for read events, disconnection events and enable edge triggered behavior for the FD.
					ctl_event.events = EPOLLIN | EPOLLRDHUP | EPOLLET;
					if(epoll_ctl(m_epoll_fd, EPOLL_CTL_ADD, infd, &ctl_event) == -1) {
						on_run_error("epoll_ctl");
					}
				}
			}
			else
			{
				// We have data on the fd waiting to be read. Read and display it.
				// We must read whatever data is available completely, as we are running in edge-triggered mode
				// and won't get a notification again for the same data.
				bool should_close = false, done = false;

				while(!done)
				{
					ssize_t count = read(event->data.fd, data, sizeof(data));
					if(count == -1)
					{
						// EAGAIN or EWOULDBLOCK means we have no more data that can be read.
						// Everything else is a real error.
						if(!(errno == EAGAIN || errno == EWOULDBLOCK))
						{
							on_run_error("read");
							should_close = true;
						}
						done = true;
					}
					else if(count == 0)
					{
						// Technically we don't need to handle this here, since we wait for EPOLLRDHUP. We handle it just to be sure.
						// End of file. The remote has closed the connection.
						should_close = true;
						done = true;
					}
					else
					{
						IOBuffer* io_buffer = io_buffers[(uint32_t)event->data.fd % thread_count];
						auto p = try_write_io_buffer(io_buffer, (uint32_t)count);
						if(p)
						{
							memcpy(p, data, count);
							write_io_buffer(io_buffer);
							on_data(event->data.fd, io_buffer);
						}
					}
				}

				if(should_close)
				{
					// Closing the descriptor will make epoll remove it from the set of descriptors which are monitored.
					close(event->data.fd);
					on_disconnect(event->data.fd);
				}
			}
		}
	}
#endif	// __linux__

#ifdef _WIN32

	DWORD			bytes = 0;
	DWORD			err_no = 0;
	IOCPContext* iocp_accept_ctx = nullptr;
	IOCPContext* iocp_ctx = nullptr;
	void* completion_key = nullptr;
	OVERLAPPED* ol = NULL;

	iocp_accept_ctx = new IOCPContext();
	iocp_post_accept(iocp_accept_ctx);

	while(!m_exit)
	{
		if(FALSE == GetQueuedCompletionStatus(m_iocp, &bytes, (PULONG_PTR)&completion_key, &ol, INFINITE))
		{
			iocp_ctx = CONTAINING_RECORD(ol, IOCPContext, overlapped);

			err_no = GetLastError();
			if(WAIT_TIMEOUT == err_no) {
				continue;
			}

			if(iocp_ctx == nullptr) {
				continue;
			}

			if(ERROR_NETNAME_DELETED == err_no || ERROR_OPERATION_ABORTED == err_no)
			{
				close_connection(iocp_ctx);
				continue;
			}

			if(err_no)
			{
				close_connection(iocp_ctx);
				break;
			}
		}

		iocp_ctx = CONTAINING_RECORD(ol, IOCPContext, overlapped);

		if(nullptr == iocp_ctx) {
			continue;
		}

		if(0 == bytes && IOCP_ACTION_READ == iocp_ctx->action)
		{
			close_connection(iocp_ctx);
			continue;
		}

		if(iocp_ctx->action == IOCP_ACTION_ACCEPT)
		{
			auto iocp_rw_ctx = iocp_bind_incoming(iocp_ctx);

			// wait for recv
			iocp_post_recv(iocp_rw_ctx);

			// callback on_connect
			accept_connection(iocp_ctx);

			// starts waiting for another connection request
			iocp_post_accept(iocp_ctx);
		}
		else if(iocp_ctx->action == IOCP_ACTION_READ)
		{
			IOBuffer* io_buffer = io_buffers[(uint32_t)iocp_ctx->client_fd % thread_count];
			auto p = try_write_io_buffer(io_buffer, (uint32_t)bytes);
			if(p)
			{
				memcpy(p, iocp_ctx->buffer, bytes);
				write_io_buffer(io_buffer);
				on_data(iocp_ctx->client_fd, io_buffer);
			}

			iocp_ctx->reset();
			iocp_post_recv(iocp_ctx);
		}
	}

#endif

	delete m_thread_pool;
	m_thread_pool = nullptr;

#ifdef _WIN32
	if(iocp_accept_ctx) {
		delete iocp_accept_ctx;
	}
#endif

	m_clients.clear();

#ifdef __linux__
	if(events) {
		free(events);
	}
#endif

	for(int i = 0; i < thread_count; i++)
	{
		destroy_io_buffer(io_buffers[i]);
		free(io_buffers[i]);
	}
	delete[] io_buffers;

#ifdef __linux__
	if(m_socket_fd != -1)
	{
		close(m_socket_fd);
		m_socket_fd = -1;
	}

	if(m_epoll_fd != -1)
	{
		close(m_epoll_fd);
		m_epoll_fd = -1;
	}
#endif

#ifdef _WIN32
	if(m_socket_fd != INVALID_SOCKET)
	{
		closesocket(m_socket_fd);
		m_socket_fd = INVALID_SOCKET;
	}

	if(m_iocp != INVALID_HANDLE_VALUE)
	{
		CloseHandle(m_iocp);
		m_iocp = INVALID_HANDLE_VALUE;
	}
#endif

	m_running = false;
}
