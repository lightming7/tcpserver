# TcpServer
An epoll/IOCP tcp server.
o. single thread accept and read
o. thread pool handle clients on_connect/on_data/on_disconnect callbacks

## build linux
```
mkdir -p build
cd build
cmake ..
make
```

## build win32
```
#call visual studio vcvars64.bat
mkdir -p build
cd build
cmake ..
msbuild test.sln /p:Configuration=Release
```

## test linux
```
./test

echo hello | nc 0 6666
```
