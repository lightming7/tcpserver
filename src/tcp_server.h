#pragma once

#include <stdint.h>
#include <functional>
#include <string>
#include <memory>
#include <map>
#include <thread>
#include <mutex>

#ifdef __linux__
#include <netinet/in.h>
#endif
#ifdef _WIN32
#include <winsock2.h>
#include <mswsock.h>
#endif

struct IOBuffer;
class ThreadPool;

struct IOCPContext;

#ifdef __linux__
typedef int		SocketHandle;
#endif

#ifdef _WIN32
typedef SOCKET	SocketHandle;
#endif

class TcpServerClient
{
public:
	TcpServerClient(SocketHandle server_fd, SocketHandle client_fd);
	virtual ~TcpServerClient();

	inline SocketHandle server_socket() const {
		return m_server_fd;
	}

	inline SocketHandle client_socket() const {
		return m_client_fd;
	}

	void CloseClientSocket();

	// send data for client socket
	int Send(const uint8_t* data, uint32_t data_size);

	virtual void on_connect(const std::string& host, uint16_t port);
	virtual void on_disconnect();
	virtual void on_data(IOBuffer* io_buffer);

protected:
	std::mutex 	m_mutex;

private:
	SocketHandle		m_server_fd;
	SocketHandle		m_client_fd;
};


class TcpServer
{
public:
	TcpServer();
	~TcpServer();

	bool is_running() const {
		return m_running;
	}

	// set callback to create client object
	void set_client_creator(std::function<TcpServerClient* (SocketHandle, SocketHandle)> creator);

	template <class T>
	TcpServerClient* new_creator(SocketHandle server_fd, SocketHandle client_fd) {
		return new T(server_fd, client_fd);
	}

	template <class T>
	void set_client_creator()
	{
		auto&& creator = std::bind(&TcpServer::new_creator<T>, this, std::placeholders::_1, std::placeholders::_2);
		set_client_creator(creator);
	}

	// start server
	bool start(int thread_count, uint16_t port, const std::string& host = std::string("0.0.0.0"));

	// stop server and wait until over
	void stop();

	// post stop, not wait
	void set_stop();

private:
	std::map<SocketHandle, std::shared_ptr<TcpServerClient>>		m_clients;
	std::function<TcpServerClient* (SocketHandle, SocketHandle)>		m_client_creator;

#ifdef __linux__
	int								m_epoll_fd;
#endif

#ifdef _WIN32
	LPFN_ACCEPTEX					m_fn_AcceptEx;
	LPFN_GETACCEPTEXSOCKADDRS		m_fn_GetAcceptExSockAddrs;
	HANDLE							m_iocp;

	WSAOVERLAPPED					m_accept_overlapped;
#endif

	// thread pool
	ThreadPool* m_thread_pool;

	// server listen socket
	SocketHandle					m_socket_fd;

	bool							m_running;
	bool							m_exit;

	// create tcp socket, bind and listen, create epoll
	bool init(const std::string& host, uint16_t port, std::string& error_function);

	// run loop
	void run(int thread_count);

	// log error
	void on_run_error(const char* error_function);

	// callback on client connected
	void on_connect(const std::string& host, uint16_t port, SocketHandle client_fd);

	// callback on client disconnected
	void on_disconnect(SocketHandle client_fd);

	// callback on client receive data
	void on_data(SocketHandle client_fd, IOBuffer* io_buffer);

	// create client object
	TcpServerClient* new_client(SocketHandle client_fd);

#ifdef _WIN32
	// bind incoming socket recv with iocp
	IOCPContext* iocp_bind_incoming(IOCPContext* iocp_ctx);

	// accept iocp connection
	void accept_connection(IOCPContext* iocp_ctx);

	// close iocp connection and free object
	void close_connection(IOCPContext* iocp_ctx);

	// bind server socket accept with IOCP
	bool iocp_post_accept(IOCPContext* iocp_ctx);

	// bind client socket recv with IOCP
	bool iocp_post_recv(IOCPContext* iocp_ctx);
#endif
};
